# Modelo Issue 4MTI

## Implementação no projeto ##

 - Crie Na raiz do projeto o diretório: .gitlab e dentro dele o diretório issue_templates <br>



<img src="https://gitlab.com/viinioliveira_/modeloissue/-/raw/master/Exemplos/Capturar.PNG" /><br>


 
- dentro de issue_templates adicione o arquivo 'Modelo_issue' dispónivel no link abaixo

> https://gitlab.com/viinioliveira_/modeloissue/-/blob/master/.gitlab/issue_templates/Modelo_Issue.md

- Pronto! o modelo já pode ser usado no projeto

## Usando o template ##

- Acesse a tela de abertura de issue e clique no campo 'Description'

<img src="https://gitlab.com/viinioliveira_/modeloissue/-/raw/master/Exemplos/template.PNG" />

- Selecione 'Modelo_issue'

<img src="https://gitlab.com/viinioliveira_/modeloissue/-/raw/master/Exemplos/template2.PNG"/>


- Pronto! o modelo será exibido na tela e estara pronto para ser editado
